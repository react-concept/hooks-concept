import React, { useContext } from "react";
import { ThemeContext } from "../pages/UseContext";

const UseContextChild = () => {
  const { darkTheme, setDarkTheme } = useContext(ThemeContext);
  return (
    <>
      <div
        className="h-32 w-[75%] border-2 border-gray-400 flex justify-center items-center rounded-lg"
        style={{
          backgroundColor: darkTheme ? "black" : "white",
          color: darkTheme ? "white" : "black",
        }}
      >
        The theme is :
        <span className="text-red-500 pl-2 font-bold">
          {darkTheme === true ? "Dark" : "Light"}
        </span>
      </div>
      <button
        className="m-2 p-2 border-2 border-gray-400 text-white rounded-xl bg-gray-400"
        onClick={() => setDarkTheme(!darkTheme)}
      >
        Change To Light Theme
      </button>
    </>
  );
};

export default UseContextChild;
