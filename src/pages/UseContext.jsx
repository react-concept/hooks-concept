import React, { useState } from "react";
import UseContextChild from "../Components/UseContextChild";

// Create context and export it.
export const ThemeContext = React.createContext();

const UseContext = () => {
  const [darkTheme, setDarkTheme] = useState(false);
  return (
    <div
      className={`h-[95vh] bg-gray-300 flex flex-col items-center justify-center`}
    >
      <p className="border-b-2 border-gray-400 mb-2">UseContext</p>
      {/* Pass the child component inside the context wrap */}
      <ThemeContext.Provider value={{ darkTheme, setDarkTheme }}>
        <UseContextChild />
      </ThemeContext.Provider>
    </div>
  );
};

export default UseContext;
