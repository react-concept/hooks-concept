import React, { useReducer } from "react";

function reducer(state, action) {
  switch (action.type) {
    case "increment":
      return { count: state.count + 1 };
    case "decrement":
      return { count: state.count - 1 };
    case "addAmount":
      return { count: state.count + action.payload.amount };
    case "reset":
      return { count: 0 };
    default:
      return state;
  }
}

const UseReducer = () => {
  const [state, dispatch] = useReducer(reducer, { count: 0 });
  return (
    <div
      className={`h-[95vh] bg-gray-300 flex flex-col items-center justify-center`}
    >
      <p className="border-b-2 border-gray-400 mb-2">UseReducer</p>

      <div className="flex gap-3 mt-2">
        <button
          className="border-2 border-black px-3 bg-red-500"
          onClick={() => dispatch({ type: "decrement" })}
        >
          -
        </button>
        <div>{state.count}</div>
        <button
          className="border-2 border-black px-3 bg-green-500"
          onClick={() => dispatch({ type: "increment" })}
        >
          +
        </button>
      </div>
      <div className="flex flex-col mt-2">
        <button
          className="border-2 border-black px-3 bg-green-400"
          onClick={() =>
            dispatch({ type: "addAmount", payload: { amount: 5 } })
          }
        >
          +5
        </button>
        <button
          className="mt-2 border-2 border-black px-3 bg-gray-400"
          onClick={() => dispatch({ type: "reset" })}
        >
          Reset
        </button>
      </div>
    </div>
  );
};

export default UseReducer;
