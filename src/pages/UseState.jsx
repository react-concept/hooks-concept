import React, { useState } from "react";

function stateInitial() {
  console.log("Hello");
  return {
    count: 4,
    color: "gray",
  };
}
const UseState = () => {
  const [state, setState] = useState(stateInitial);

  const increment = () => {
    setState((prevState) => {
      return {
        count: prevState.count + 1,
        color: "green",
      };
    });
  };

  const decrement = () => {
    setState((prevState) => {
      return {
        count: prevState.count - 1,
        color: "red",
      };
    });
  };

  return (
    <div className={`h-[95vh] bg-gray-300 flex items-center justify-center`}>
      <div className="flex gap-5">
        <button
          className="border-2 border-black px-3 bg-red-500"
          onClick={decrement}
        >
          -
        </button>
        <div>{state?.count}</div>
        <div className={`border-2 border-black px-2`}>{state?.color}</div>
        <button
          className="border-2 border-black px-3 bg-green-500"
          onClick={increment}
        >
          +
        </button>
      </div>
    </div>
  );
};

export default UseState;
