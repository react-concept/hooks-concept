import React, { useCallback, useMemo } from "react";

const UseMemoVsUseCallback = () => {
  function foo() {
    return "Hello Zaffy";
  }

  const memoizedCallback = useCallback(foo, []);
  const memoizedResult = useMemo(foo, []);
  return (
    <div
      className={`h-[95vh] bg-gray-300 flex flex-col items-center justify-center`}
    >
      <p className="border-b-2 border-gray-400 mb-2">UseMemoVsUseCallback</p>
      <p>
        <span className="font-bold">useCallback:</span> Returns a memoized
        callback.
      </p>
      <p>
        <span className="font-bold">useMemo:</span> Returns a memoized value.
      </p>
      <p className="text-center">
        useCallback returns its function uncalled so you can call it later,
        while useMemo calls its function and returns the result.
      </p>
      <div>{memoizedCallback()}</div>
      <div>{memoizedResult}</div>
    </div>
  );
};

export default UseMemoVsUseCallback;
