import React, { useMemo, useState } from "react";

const UseMemo = () => {
  const [input, setInput] = useState(0);
  const [darkTheme, setDarkTheme] = useState(false);

  const doubleNumber = useMemo(() => {
    return slowFunction(input);
  }, [input]);

  // useMemo prevent to re-render the themeColor when a new input given
  const themeColor = useMemo(() => {
    return {
      backgroundColor: darkTheme ? "black" : "white",
      color: darkTheme ? "white" : "black",
    };
  }, [darkTheme]);

  return (
    <div
      className={`h-[95vh] bg-gray-300 flex flex-col items-center justify-center`}
    >
      <p className="border-b-2 border-gray-400 mb-2">UseMemo</p>
      <input
        className="h-10 rounded-xl px-2 border-2 border-gray-400 mb-2"
        type="number"
        onChange={(e) => setInput(e.target.value)}
      />
      <button
        className="mb-2 p-2 border-2 border-gray-400 rounded-xl bg-gray-400"
        onClick={() => setDarkTheme((prevState) => !prevState)}
      >
        Change Thames
      </button>
      <div
        style={themeColor}
        className="h-10 w-[150px] p-2 border-2 border-gray-400 rounded-xl"
      >
        {doubleNumber}
      </div>
    </div>
  );
};

function slowFunction(param) {
  console.log("Calling Slow Function...");
  for (let index = 0; index < 1000000000; index++) {}
  return param * 2;
}

export default UseMemo;

// Learn Here : https://blog.webdevsimplified.com/2020-05/memoization-in-react/
