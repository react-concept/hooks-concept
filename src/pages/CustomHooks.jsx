import React from "react";
import useLocalStorage from "../CustomHook/useLocalStorage";

const CustomHooks = () => {
  const [value, setValue] = useLocalStorage("name", "");
  return (
    <div
      className={`h-[95vh] bg-gray-300 flex flex-col items-center justify-center`}
    >
      <p className="border-b-2 border-gray-400 mb-2">
        CustomHooks(useLocalStorage)
      </p>
      <div>
        <label htmlFor="name">Name : </label>
        <input
          className="h-10 rounded-xl px-2 border-2 border-gray-400 mb-2"
          type="text"
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
      </div>
    </div>
  );
};

export default CustomHooks;
