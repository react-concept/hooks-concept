import React, { useCallback, useState } from "react";
import List from "../Components/List";

const UseCallback = () => {
  const [input, setInput] = useState(1);
  const [darkTheme, setDarkTheme] = useState(false);

  // If remove useCallback when we change themeColor getItems re-render
  const getItems = useCallback(() => {
    let data = parseInt(input);
    return [data, data + 1, data + 2];
  }, [input]);

  const themeColor = {
    backgroundColor: darkTheme ? "black" : "white",
    color: darkTheme ? "white" : "black",
  };

  return (
    <div
      className={`h-[95vh] bg-gray-300 flex flex-col items-center justify-center`}
    >
      <p className="border-b-2 border-gray-400 mb-2">UseCallback</p>
      <div
        className="w-[70%] h-64 border-2 border-gray-400 flex flex-col items-center justify-center"
        style={themeColor}
      >
        <input
          className="h-10 rounded-xl px-2 border-2 border-gray-400 mb-2"
          type="number"
          value={input}
          onChange={(e) => setInput(e.target.value)}
          style={{ color: "black" }}
        />
        <button
          className="mb-2 p-2 border-2 border-gray-400 rounded-xl bg-gray-400"
          onClick={() => setDarkTheme((prevState) => !prevState)}
        >
          Change Thames
        </button>
        <List getItems={getItems} />
      </div>
    </div>
  );
};

export default UseCallback;

// https://blog.webdevsimplified.com/2020-05/memoization-in-react/

/* Main difference is that useMemo will call the function passed to it whenever its dependencies change and will return the value of that function call. useCallback on the other hand will not call the function passed to it and instead will return a new version of the function passed to it whenever the dependencies change. This means that as long as the dependencies do not change then useCallback will return the same function as before which maintains referential equality.*/

/*
useCallback(() => {
  return a + b
}, [a, b])

useMemo(() => {
  return () => a + b
}, [a, b])
*/
