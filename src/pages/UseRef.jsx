import React, { useEffect, useRef, useState } from "react";

const UseRef = () => {
  // State Render Infinite times.
  // const [rerenderCount, setRerenderCount] = useState(0);
  // useEffect(() => {
  //   setRerenderCount((prevCount) => prevCount + 1);
  // });

  // Mostly for input value we use useRef()
  const rerenderCountRef = useRef(0);

  useEffect(() => {
    rerenderCountRef.current = rerenderCountRef.current + 1;
  });

  return (
    <div
      className={`h-[95vh] bg-gray-300 flex flex-col items-center justify-center`}
    >
      <p className="border-b-2 border-gray-400">UseRef Concept</p>
      {/* <div>useState : {rerenderCount}</div> */}
      <div>useRef : {rerenderCountRef.current}</div>
    </div>
  );
};

export default UseRef;
