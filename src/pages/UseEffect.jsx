import React, { useEffect, useState } from "react";

const UseEffect = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    window.addEventListener("resize", updateWindowWidth);
    return () => {
      // Clean-up Function
      window.removeEventListener("resize", updateWindowWidth);
    };
  }, []);

  function updateWindowWidth() {
    setWindowWidth(window.innerWidth);
  }

  return (
    <div className={`h-[95vh] bg-gray-300 flex items-center justify-center`}>
      <div className="font-bold">
        Width of thw Window :{" "}
        <span className="text-red-500">{windowWidth}</span> PX
      </div>
    </div>
  );
};

export default UseEffect;
