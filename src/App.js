import React from "react";
import { Link, Route, Routes } from "react-router-dom";
import UseState from "./pages/UseState";
import UseEffect from "./pages/UseEffect";
import UseRef from "./pages/UseRef";
import UseCallback from "./pages/UseCallback";
import UseMemo from "./pages/UseMemo";
import UseReducer from "./pages/UseReducer";
import UseContext from "./pages/UseContext";
import CustomHooks from "./pages/CustomHooks";
import UseMemoVsUseCallback from "./pages/UseMemoVsUseCallback";

const Home = () => {
  return (
    <div className="bg-emerald-200 w-full h-[95vh] flex justify-center items-center">
      <h1 className="px-5 text-gray-500 font-bold text-center">
        Learn React-Router-Dom V6, & React-Hooks (useState, useEffect,
        useCallback, useMemo, useReducer, & useRef).
      </h1>
    </div>
  );
};

const NotFound = () => {
  return (
    <div className="bg-gray-200 w-full h-[95vh] flex justify-center items-center">
      <h1 className="px-5 text-gray-500 font-bold">404 Not Found!</h1>
    </div>
  );
};

function App() {
  return (
    <>
      <nav className="w-full h-auto py-4 flex justify-center bg-blue-200">
        <ul className="flex flex-wrap justify-center items-center gap-4">
          <li className="font-bold">
            <Link to="/">Home</Link>
          </li>
          <li className="font-bold">
            <Link to="/useState">useState</Link>
          </li>
          <li className="font-bold">
            <Link to="/useEffect">useEffect</Link>
          </li>
          <li className="font-bold">
            <Link to="/useRef">useRef</Link>
          </li>
          <li className="font-bold">
            <Link to="/useMemo">useMemo</Link>
          </li>
          <li className="font-bold">
            <Link to="/useCallback">useCallback</Link>
          </li>
          <li className="font-bold">
            <Link to="/useMemo-useCallback">useMemo-useCallback</Link>
          </li>
          <li className="font-bold">
            <Link to="/useContext">useContext</Link>
          </li>
          <li className="font-bold">
            <Link to="/useReducer">useReducer</Link>
          </li>
          <li className="font-bold">
            <Link to="/customHooks">customHooks</Link>
          </li>
        </ul>
      </nav>

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/useState" element={<UseState />} />
        <Route path="/useEffect" element={<UseEffect />} />
        <Route path="/useRef" element={<UseRef />} />
        <Route path="/useCallback" element={<UseCallback />} />
        <Route path="/useMemo" element={<UseMemo />} />
        <Route path="/useMemo-useCallback" element={<UseMemoVsUseCallback />} />
        <Route path="/useReducer" element={<UseReducer />} />
        <Route path="/useContext" element={<UseContext />} />
        <Route path="/customHooks" element={<CustomHooks />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
    </>
  );
}

export default App;
